package de.axelrindle.parkplatz.controller;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@RestController
public class FaviconController {

    private static String getMimeType(String ext) {
        switch (ext) {
            case "png": return "image/png";
            case "ico": return "image/x-icon";
            default: return null;
        }
    }

    private void send(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ClassPathResource favicon = new ClassPathResource("static/img/favicon.png");
        String mime = getMimeType( favicon.getFilename().split("\\.")[1] );
        response.setContentType(mime);

        try (
                InputStream is = favicon.getInputStream();
                OutputStream os = response.getOutputStream()
        ) {
            IOUtils.copy(is, os);
            os.flush();
        }
    }

    @GetMapping("/favicon.png")
    public void faviconPng(HttpServletRequest request, HttpServletResponse response) throws IOException {
        send(request, response);
    }

    @GetMapping("/favicon.ico")
    public void faviconIco(HttpServletRequest request, HttpServletResponse response) throws IOException {
        send(request, response);
    }
}
