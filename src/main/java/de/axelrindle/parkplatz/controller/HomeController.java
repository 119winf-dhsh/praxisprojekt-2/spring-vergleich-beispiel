package de.axelrindle.parkplatz.controller;

import de.axelrindle.parkplatz.entity.User;
import de.axelrindle.parkplatz.repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
public class HomeController {

    private final UserRepository userRepository;

    public HomeController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/")
    public ModelAndView index() {
        Iterable<User> users = userRepository.findAll();

        Map<String, Object> options = new HashMap<>();
        options.put("users", users);
        return new ModelAndView("index", options);
    }

    @GetMapping("/createUser")
    public ResponseEntity<String> createUser(
            @RequestParam("username") Optional<String> username,
            @RequestParam("email") Optional<String> email
    ) {
        if (! username.isPresent()) return ResponseEntity.status(400).body("Username not found!");
        if (! email.isPresent()) return ResponseEntity.status(400).body("Email not found!");

        if (userRepository.findByUsername(username.get()) != null) return ResponseEntity.status(400).body("Duplicate username!");
        if (userRepository.findByEmail(username.get()) != null) return ResponseEntity.status(400).body("Duplicate email!");

        User user = new User(
                username.get(),
                email.get()
        );
        userRepository.save(user);
        return ResponseEntity.ok("OK");
    }
}
